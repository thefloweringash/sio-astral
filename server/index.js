const io = require('socket.io')(process.env.PORT || 3500);

io.on('connection', (client) => {
    const test = (c, s) => {
        console.log(`Sending: class ${c} string ${s}`);
        client.emit('msg', { c, s }, (ack) => {
            console.log(`Client reply: class ${c} string ${s}: got ${ack}`);
        });
    };

    test('Astral', '\uD83D\uDE06');
    test('Polish', 'ł');
    test('Japanese', '懐かしい');
    test('socketio/socket.io#3126 1', '≥');
    test('socketio/socket.io#3126 2', '\u2265');
});
