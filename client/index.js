const io = require('socket.io-client');

io.connect('http://localhost:3500/', {
    path: '/socket.io',
    transports: ['polling'],
}).on('msg', ({ c, s }, ack) => {
    console.log(`Received c:${c} s:${s}`);
    ack(s);
});
